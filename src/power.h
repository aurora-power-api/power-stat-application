/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <QDateTime>
#include <QMap>
#include <QObject>

class Power : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QDateTime lastFullCharged READ lastFullCharged CONSTANT)
    Q_PROPERTY(QDateTime deviceTimeLeft READ deviceTimeLeft CONSTANT)

public:
    explicit Power(QObject *parent = NULL);
    ~Power() = default;

    QDateTime lastFullCharged() const;
    QDateTime deviceTimeLeft() const;
    QMap<QDateTime, int> batteryLevelHistory() const;
};
