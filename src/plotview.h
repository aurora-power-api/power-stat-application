/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <QColor>
#include <QQuickItem>
#include <QList>
#include <QSGNode>
#include <QSGGeometryNode>
#include <QScopedPointer>

#include <map>

class PlotView: public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(int verticalLinesNumber READ verticalLinesNumber WRITE setVerticalLinesNumber NOTIFY verticalLinesNumberChanged)
    Q_PROPERTY(int horizontalLinesNumber READ horizontalLinesNumber WRITE setHorizontalLinesNumber NOTIFY horizontalLinesNumberChanged)
    Q_PROPERTY(QColor primaryColor READ primaryColor WRITE setPrimaryColor NOTIFY primaryColorChanged)
    Q_PROPERTY(QColor secondaryColor READ primaryColor WRITE setSecondaryColor NOTIFY secondaryColorChanged)
    Q_PROPERTY(int dotLineSize READ dotLinesSize WRITE setDotLinesSize NOTIFY dotLinesSizeChanged)

public:
    explicit PlotView(QQuickItem *parent = NULL);
    ~PlotView();

    // Drawing logic
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *) override;

    int verticalLinesNumber() const;
    int horizontalLinesNumber() const;
    int dotLinesSize() const;
    QColor primaryColor() const;
    QColor secondaryColor() const;

    // Property setters
    void setData(const QMap<int, int> &data);
    void setVerticalLinesNumber(int linesNumber);
    void setHorizontalLinesNumber(int linesNumber);
    void setDotLinesSize(int dotLineSize);
    void setPrimaryColor(QColor color);
    void setSecondaryColor(QColor color);

private:
    void plotGraph(QSGGeometryNode *node);
    void plotTimeLine(QSGGeometryNode *node);
    void plotBackground(QSGGeometryNode *node);
    void plotBlur(QSGGeometryNode *node);
    void plotPrediction(QSGGeometryNode *node);

    std::map<float, float> getGraphData();
    std::pair<float, float> getPredictedGraphData();

signals:
    void verticalLinesNumberChanged();
    void horizontalLinesNumberChanged();
    void dotLinesSizeChanged();
    void primaryColorChanged();
    void secondaryColorChanged();

private:
    QSGGeometryNode *m_background;

    std::map<float, float> m_graphCoords;
    int m_verticalLinesNumber;
    int m_horizontalLinesNumber;
    int m_dotLinesSize;
    QColor m_primaryColor;
    QColor m_secondaryColor;
};
