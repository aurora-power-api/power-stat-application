/*
 * Copyright 2023 Dmitriy Butakov
 */

#pragma once

#include <QDateTime>
#include <QMap>
#include <QObject>
#include <QString>

class AppInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QMap<QString, QDateTime> applicationsActivity READ applicationsActivity NOTIFY
                   applicationsActivityChanged)

public:
    explicit AppInfo(QObject *parent = NULL);
    ~AppInfo() = default;

    QMap<QString, QDateTime> applicationsActivity() const;

    Q_INVOKABLE QString lookupIconSource(const QString &applicationName) const;

signals:
    void applicationsActivityChanged();
};
