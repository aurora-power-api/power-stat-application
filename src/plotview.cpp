/*
 * Copyright 2023 Dmitriy Butakov
 */

#include <QSGSimpleRectNode>
#include <QGraphicsItem>
#include <QSGSimpleMaterial>
#include <GLES2/gl2ext.h>

#include <powerstat.h>

#include "plotview.h"
#include "power.h"

#include <chrono>

namespace {
    const int32_t MS_IN_DAY = 86400000;

    QColor getBluredColor(QColor primaryColor)
    {
        int r,g,b;
        primaryColor.getRgb(&r,&g,&b);
        return QColor(r,g,b, 64);
    }

    void setGeometry(QSGGeometryNode *node, int pointCount,
                     GLenum lineMode, float lineWidth)
    {
        QSGGeometry *geometry = new QSGGeometry(QSGGeometry::defaultAttributes_Point2D(), pointCount);
        geometry->setDrawingMode(lineMode);
        geometry->setLineWidth(lineWidth);

        node->setGeometry(geometry);
        node->setFlags(QSGNode::OwnsGeometry);
    }

    void setMaterial(QSGGeometryNode *node, QColor color, qreal opacity)
    {
        QSGFlatColorMaterial *material = new QSGFlatColorMaterial();
        material->setColor(color);

        node->setMaterial(material);
        node->setInheritedOpacity(opacity);
        node->setFlags(QSGNode::OwnsMaterial);
    }
} // namespace

PlotView::PlotView(QQuickItem *parent)
    : QQuickItem(parent),
      m_background(nullptr),
      m_verticalLinesNumber(6),
      m_horizontalLinesNumber(5),
      m_dotLinesSize(5),
      m_primaryColor(Qt::white),
      m_secondaryColor(Qt::white)
{
    setFlag(ItemHasContents);

    connect(this, &PlotView::verticalLinesNumberChanged, this, &PlotView::update);
    connect(this, &PlotView::horizontalLinesNumberChanged, this, &PlotView::update);
    connect(this, &PlotView::dotLinesSizeChanged, this, &PlotView::update);
    connect(this, &PlotView::primaryColorChanged, this, &PlotView::update);
    connect(this, &PlotView::secondaryColorChanged, this, &PlotView::update);
}

PlotView::~PlotView()
{
    m_background->removeAllChildNodes();
}

void PlotView::plotBackground(QSGGeometryNode *node)
{
    float step = 0;
    float diff = height() / (m_horizontalLinesNumber - 1);

    int i = 0;
    for(; i < 2 * m_horizontalLinesNumber; i += 2) {
        node->geometry()->vertexDataAsPoint2D()[i].set(0, step);
        node->geometry()->vertexDataAsPoint2D()[i+1].set(width(), step);
        step += diff;
    }

    // Add left & right borders
    node->geometry()->vertexDataAsPoint2D()[i].set(0, 0);
    node->geometry()->vertexDataAsPoint2D()[i+1].set(0, height());
    node->geometry()->vertexDataAsPoint2D()[i+2].set(width(), 0);
    node->geometry()->vertexDataAsPoint2D()[i+3].set(width(), height());
}

void PlotView::plotTimeLine(QSGGeometryNode *node)
{
    float y_step = 0;
    float x_diff = width() / m_verticalLinesNumber;
    // Do not count first line, since it's border
    // It is needded to count for displaying hours correctly
    float x_step = x_diff;

    int total_dots = 0;
    while (x_step < width()) {
        y_step = 0;
        while (y_step < height()) {
            node->geometry()->vertexDataAsPoint2D()[total_dots].set(x_step, y_step);
            node->geometry()->vertexDataAsPoint2D()[total_dots + 1].set(x_step, y_step + m_dotLinesSize);
            y_step += 2 * m_dotLinesSize;
            total_dots += 2;
        }
        x_step += x_diff;
    }
}

void PlotView::plotGraph(QSGGeometryNode *node)
{
    int i = 0;
    for (const auto &[x, y]: m_graphCoords)
        node->geometry()->vertexDataAsPoint2D()[i++].set(x, y);
}

void PlotView::plotBlur(QSGGeometryNode *node)
{
    int i = 0;
    for (auto it = m_graphCoords.begin(); it != std::prev(m_graphCoords.end()); it++) {
        auto currentPoint = *it;
        node->geometry()->vertexDataAsPoint2D()[i++].set(currentPoint.first, currentPoint.second);
        node->geometry()->vertexDataAsPoint2D()[i++].set(currentPoint.first, height());
        
        auto nextPoint = *std::next(it);
        node->geometry()->vertexDataAsPoint2D()[i++].set(nextPoint.first, height());
        node->geometry()->vertexDataAsPoint2D()[i++].set(nextPoint.first, nextPoint.second);
        node->geometry()->vertexDataAsPoint2D()[i++].set(currentPoint.first, currentPoint.second);
        node->geometry()->vertexDataAsPoint2D()[i++].set(nextPoint.first, height());
    }
}

void PlotView::plotPrediction(QSGGeometryNode *node)
{
    auto lastMeasutedCords = *std::prev(m_graphCoords.end());
    node->geometry()->vertexDataAsPoint2D()[0].set(lastMeasutedCords.first, lastMeasutedCords.second);
    auto predictedCoords = getPredictedGraphData();
    node->geometry()->vertexDataAsPoint2D()[1].set(predictedCoords.first, predictedCoords.second);
}

QSGNode* PlotView::updatePaintNode(QSGNode *oldNode,
                                   UpdatePaintNodeData *updatePaintNodeData)
{
    Q_UNUSED(updatePaintNodeData);

    m_background = static_cast<QSGGeometryNode *>(oldNode);
    if(!m_background) {
        m_background = new QSGGeometryNode();
        setMaterial(m_background, m_secondaryColor, 0);

        auto timeLine = new QSGGeometryNode();
        setMaterial(timeLine, m_secondaryColor, 0);
        timeLine->setFlag(QSGNode::OwnedByParent);

        m_graphCoords = getGraphData();
        auto graph = new QSGGeometryNode();
        setMaterial(graph, m_primaryColor, 0);
        graph->setFlag(QSGNode::OwnedByParent);

        auto blur = new QSGGeometryNode();
        setMaterial(blur, getBluredColor(m_primaryColor), 0.4);
        blur->setFlag(QSGNode::OwnedByParent);

        auto predictedLine =  new QSGGeometryNode();
        setMaterial(predictedLine, m_primaryColor, 0);
        predictedLine->setFlag(QSGNode::OwnedByParent);

        m_background->appendChildNode(timeLine);
        m_background->appendChildNode(graph);
        m_background->appendChildNode(blur);
        m_background->appendChildNode(predictedLine);
    }
    // No need to reallocated material every time
    // Multiply by two to pass number of points and add right adn left border
    setGeometry(m_background, (m_horizontalLinesNumber * 2 + 4), GL_LINES, 1);
    plotBackground(m_background);

    int dottedLinesNumber = static_cast<int>(height() / static_cast<float>(m_dotLinesSize)) / 2;
    auto timeLine = static_cast<QSGGeometryNode *>(m_background->firstChild());
    setGeometry(timeLine, (m_verticalLinesNumber - 1) * dottedLinesNumber * 2, GL_LINES, 1);
    plotTimeLine(timeLine);

    if (!m_graphCoords.empty()) {
        auto graph = static_cast<QSGGeometryNode *>(timeLine->nextSibling());
        qDebug() << m_graphCoords.size();
        setGeometry(graph, m_graphCoords.size(), GL_LINE_STRIP, 1);
        plotGraph(graph);

        auto blur = static_cast<QSGGeometryNode *>(graph->nextSibling());
        setGeometry(blur, 6 * m_graphCoords.size() - 6, GL_TRIANGLES, 1);
        plotBlur(blur);

        auto predictedLine = static_cast<QSGGeometryNode *>(blur->nextSibling());
        setGeometry(predictedLine, 2, GL_LINES, 1);
        plotPrediction(predictedLine);
    }
    // No need to call markDirty because setGeometry is called.

    return m_background;
}

std::map<float, float> PlotView::getGraphData()
{
    auto batteryHistory = Aurora::Power::PowerStat::batteryDischargeHistory();
    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
    // Find first measurement within 24 hours
    auto firstStart = std::find_if(batteryHistory.begin(), batteryHistory.end(), [now](const auto &e){
        return e.first > now - MS_IN_DAY;
    });

    std::map<float, float> graphData;
    float historyWidth = width() * (m_horizontalLinesNumber - 1) / m_horizontalLinesNumber;
    float step = historyWidth / (std::distance(firstStart, batteryHistory.end()) - 1);
    float x = 0;
    int h = height();
    for (auto it = firstStart; it != batteryHistory.end(); ++it) {
        float y = h - (static_cast<float>(it->second) / 100) * h;
        if (y < 0)
            continue;

        qDebug() << "it->second = " << it->second << ": x = " << x << ", y = " << y;
        graphData[x] = y;
        x += step;
    }
    qDebug() << "Finished";
    return graphData;
}

std::pair<float, float> PlotView::getPredictedGraphData()
{
    uint64_t prediction = Aurora::Power::PowerStat::battryTimeToDischarge();
    float predicted_width = width() / m_horizontalLinesNumber;
    float visible_width = predicted_width * (m_horizontalLinesNumber - 1);
    float x = prediction / (4 * 60 * 60 * 1000 * 1000); // 4 hours in ms
    if (x < predicted_width)
        return std::make_pair(visible_width + x, height());

    auto lastCoords = *std::prev(m_graphCoords.end());
    float y = (x / lastCoords.first) * lastCoords.second;
    return std::make_pair(width(), y);
}

int PlotView::verticalLinesNumber() const
{
    return m_verticalLinesNumber;
}

int PlotView::horizontalLinesNumber() const
{
    return m_horizontalLinesNumber;
}

int PlotView::dotLinesSize() const
{
    return m_dotLinesSize;
}

QColor PlotView::primaryColor() const
{
    return m_primaryColor;
}

QColor PlotView::secondaryColor() const
{
    return m_secondaryColor;
}

void PlotView::setVerticalLinesNumber(int linesNumber)
{
    if (m_verticalLinesNumber == linesNumber)
        return;

    m_verticalLinesNumber = linesNumber;
    emit verticalLinesNumberChanged();
}

void PlotView::setHorizontalLinesNumber(int linesNumber)
{
    if (m_horizontalLinesNumber == linesNumber)
        return;

    m_horizontalLinesNumber = linesNumber;
    emit horizontalLinesNumberChanged();
}

void PlotView::setDotLinesSize(int dotLinesSize)
{
    if (m_dotLinesSize == dotLinesSize)
        return;
    
    m_dotLinesSize = dotLinesSize;
    emit dotLinesSizeChanged();
}

void PlotView::setPrimaryColor(QColor primaryColor)
{
    if (m_primaryColor == primaryColor)
        return;

    m_primaryColor = primaryColor;
    emit primaryColorChanged();
}

void PlotView::setSecondaryColor(QColor secondaryColor)
{
    if (m_secondaryColor == secondaryColor)
        return;

    m_secondaryColor = secondaryColor;
    emit secondaryColorChanged();
}
