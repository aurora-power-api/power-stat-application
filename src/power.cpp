/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "power.h"

#include <powerstat.h>

#include <QDBusConnection>
#include <QDebug>
#include <QDir>
#include <QFile>

#include <map>

Power::Power(QObject *parent)
    : QObject(parent)
{}

QDateTime Power::lastFullCharged() const
{
    return QDateTime::fromTime_t(Aurora::Power::PowerStat::lastFullChargedTime());
}

QDateTime Power::deviceTimeLeft() const
{
    return QDateTime::fromTime_t(Aurora::Power::PowerStat::battryTimeToDischarge());
}

