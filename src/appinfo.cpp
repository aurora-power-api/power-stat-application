/*
 * Copyright 2023 Dmitriy Butakov
 */

#include "appinfo.h"

#include <QDBusConnection>
#include <QDBusReply>
#include <QDebug>
#include <QFile>
#include <QRegExp>
#include <QString>

namespace {
const QString &APPSTAT_SERVICE = QStringLiteral("ru.omp.AppStatService");
const QString &APPSTAT_OBJECT = QStringLiteral("/ru/omp/AppStatService");
const QString &APPSTAT_INTERFACE = QStringLiteral("ru.omp.AppStatService");
const QString &APPSTAT_ACTIVIY_METHOD = QStringLiteral("getApplicationsActivity");
} // namespace

AppInfo::AppInfo(QObject *parent)
    : QObject(parent)
{}

// Assume, that all applications provided info in .desktop file
QString AppInfo::lookupIconSource(const QString &applicationName) const
{
    QString iconPath = QStringLiteral("image://theme/");
    QString desktopPath = QStringLiteral("/usr/share/applications/") + applicationName
                          + QStringLiteral(".desktop");
    if (!QFile::exists(desktopPath)) {
        qWarning() << "No match for" << desktopPath;
        return iconPath.append(QStringLiteral("icon-system-warning"));
    }

    QFile appDesktopFile(desktopPath);
    appDesktopFile.open(QIODevice::ReadOnly);
    QTextStream in(&appDesktopFile);
    while (!in.atEnd()) {
        QString currentLine = in.readLine();
        if (currentLine.startsWith(QStringLiteral("Icon="))) {
            appDesktopFile.close();
            return iconPath.append(currentLine.split("=")[1]);
        }
    }
    appDesktopFile.close();

    qWarning() << "No icon provided for" << desktopPath;
    return iconPath.append(QStringLiteral("icon-system-warning"));
}

QMap<QString, QDateTime> AppInfo::applicationsActivity() const
{
    QMap<QString, QDateTime> appActivity;

    auto systemBus = QDBusConnection::systemBus();
    if (!systemBus.isConnected()) {
        qWarning() << "Failed to connect to system bus";
        return appActivity;
    }

    auto message = QDBusMessage::createMethodCall(APPSTAT_SERVICE,
                                                  APPSTAT_OBJECT,
                                                  APPSTAT_INTERFACE,
                                                  APPSTAT_ACTIVIY_METHOD);

    QDBusReply<QMap<QString, int>> reply = systemBus.call(message);
    if (!reply.isValid()) {
        qWarning() << "Reply failed:" << reply.error().message();
        return appActivity;
    }

    for (const auto &[key, value] : reply.value().toStdMap()) {
        appActivity[key] = QDateTime::fromMSecsSinceEpoch(value);
    }

    return appActivity;
}
