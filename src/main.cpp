/*
 * Copyright 2023 Dmitriy Butakov
 */

#include <auroraapp.h>
#include <QtQml>
#include <QtQuick>

#include "appinfo.h"
#include "plotview.h"
#include "power.h"

int main(int argc, char *argv[])
{
    qmlRegisterType<PlotView>("powerapp.helpers", 1, 0, "PlotView");
    qmlRegisterType<AppInfo>("powerapp.helpers", 1, 0, "AppInfo");
    qmlRegisterType<Power>("powerapp.helpers", 1, 0, "Power");

    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setApplicationName(QStringLiteral("powerapp"));

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/PowerApp.qml")));
    view->show();

    return application->exec();
}
