Name:    powerapp
Summary: Application for displaying information about battery usage
Version: 0.1.0
Release: 1
License: Apache 2.0
Source0: %{name}-%{version}.tar.zst
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(powerstat)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Gui)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(Qt5Widgets)
Requires:       sailfishsilica-qt5 >= 0.10.9
Requires:       powerstat

%description
%{summary}.

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%files
%{_bindir}/%{name}
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
