/*******************************************************************************
**
** Copyright 2023 Dmitriy Butakov.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Lipstick 1.0
import Sailfish.Silica 1.0
import powerapp.helpers 1.0

Page {
    id: root

    readonly property int numberOfTimeStamps: plot.verticalLinesNumber
    readonly property int timeStep: 24 / numberOfTimeStamps
    readonly property var currnetTime: Qt.formatTime(new Date())
    readonly property var timeSinceLastCharge: powerInfo.lastFullCharged === 0 ? qsTr("Never") : 
                                               powerInfo.lastFullCharged.toLocaleTimeString(Qt.locale(), Locale.ShortFormat)

    property bool userStatisticsEnabled

    function getTimeLeft(ms) {
        var mins = Math.floor(ms / (1000 * 1000 * 60))
        var hours = Math.floor(mins / 60)
        if (hours == 0) {
            return qsTr("%1m").arg(mins)
        }
        var days = Math.floor(hours / 24)
        mins = hours % 60
        if (days == 0) {
            return qsTr("%1h %2m").arg(hours).arg(mins)
        }
        if (days == 1) {
            return qsTr("1 day %2h %3m").arg(hours).arg(mins)
        }
        hours = days % 24
        return qsTr("%1 days %2h %3m").arg(days).arg(hours).arg(mins)
    }

    function prependZero(string) {
        return string.length < 2 ? '0' + string : string
    }

    function getTimeLine() {
        var currnetHour = parseInt(currnetTime, "hh")
        // Displaying only even numbers
        var markedHour = (currnetHour % 2 == 0 ? currnetHour : currnetHour - 1)
        var timeLine = []
        for (var i = numberOfTimeStamps - 2; i > 0 ; i--) {
            var prevHour = markedHour - timeStep * i
            prevHour = prevHour >= 0 ? prevHour : 24 + prevHour
            timeLine.push(prependZero(prevHour.toString()))
        }
        timeLine.push(prependZero(markedHour.toString()))

        var pastHour = markedHour + timeStep > 24 ? markedHour + timeStep - 24
                                                  : markedHour + timeStep;
        timeLine.push(pastHour.toString())

        return timeLine
    }

    AppInfo {
        id: appInfo
    }

    Power {
        id: powerInfo
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: content.height + 2 * Theme.paddingLarge

        Column {
            id: content

            width: parent.width

            PageHeader {
                title: qsTr("Battery")
            }

            Column {
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }

                Label {
                    color: Theme.primaryColor
                    font {
                        pixelSize: Theme.fontSizeLarge
                        family: Theme.fontFamily
                    }
                    text: qsTr("Last charged to 100%")
                }

                Label {
                    color: Theme.secondaryColor
                    font {
                        pixelSize: Theme.fontSizeMedium
                        family: Theme.fontFamily
                    }
                    text: timeSinceLastCharge
                }

                Item { width: 1; height: Theme.paddingMedium}

                Label {
                    color: Theme.primaryColor
                    font {
                        pixelSize: Theme.fontSizeLarge
                        family: Theme.fontFamily
                    }
                    text: qsTr("Time left")
                }

                Label {
                    color: Theme.secondaryColor
                    font {
                        pixelSize: Theme.fontSizeMedium
                        family: Theme.fontFamily
                    }
                    text: getTimeLeft(powerInfo.deviceTimeLeft)
                }
            }

            SectionHeader {
                id: sectionHeader

                text: qsTr("Battery statistics")
            }

            Item {width: 1; height: Theme.paddingLarge}

            Column {
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }

                Row {
                    id: plotView

                    spacing: Theme.paddingSmall

                    PlotView {
                        id: plot

                        width: Screen.width * 0.8
                        height: 400
                        primaryColor: Theme.primaryColor
                        secondaryColor: Theme.secondaryColor
                    }

                    Column {
                        id: percentage

                        height: plot.height
                        spacing: height / 3 + Theme.fontSizeExtraSmall

                        Repeater {
                            model: ['100%', '50%', '0%']
                            delegate: Label {
                                text: modelData
                                color: Theme.primaryColor
                                font {
                                    pixelSize: Theme.fontSizeExtraSmall
                                    family: Theme.fontFamily
                                }
                            }
                        }
                    }
                }

                Row {
                    id: time

                    width: plot.width
                    spacing: width / numberOfTimeStamps - Theme.fontSizeExtraSmall

                    Repeater {
                        model: root.getTimeLine()
                        delegate: Label {
                            text: modelData
                            color: Theme.secondaryColor
                            font {
                                pixelSize: Theme.fontSizeExtraSmall
                                family: Theme.fontFamily
                            }
                        }
                    }
                }
            }

            Item {width: 1; height: Theme.paddingLarge}

            IconTextSwitch {
                id: switchItem

                onClicked: userStatisticsEnabled = !userStatisticsEnabled

                anchors.horizontalCenter: parent.horizontalCenter
                text: userStatisticsEnabled ? qsTr("Disable user statistics collection")
                                            : qsTr("Enable user statistics collection")
                description: qsTr("For better battery estimation, user statistics should be considered")
            }

            SectionHeader {
                id: applicationHeader

                text: qsTr("Application statistics")
                visible: userStatisticsEnabled
            }

            ComboBox {
                id: statisticsBox

                visible: userStatisticsEnabled
                label: qsTr("Show")
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Battery Usage")
                    }
                    MenuItem {
                        text: qsTr("Applications Activity")
                    }
                }
            }

            Item {width: 1; height: Theme.paddingMedium}

            Column {
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }
                spacing: Theme.paddingMedium
                visible: userStatisticsEnabled

                Repeater {
                    model: ["sailfish-browser", "jolla-calendar", "jolla-email"]
                    delegate: Row {
                        spacing: Theme.paddingMedium

                        LauncherIcon {
                            id: appIcon

                            icon: appInfo.lookupIconSource(modelData)
                        }

                        Label {
                            id: appName

                            anchors.verticalCenter: parent.verticalCenter
                            text: modelData
                            color: Theme.primaryColor
                            font {
                                pixelSize: Theme.fontSizeLarge
                                family: Theme.fontFamily
                            }
                        }

                        Label {
                            id: percentage

                            anchors.verticalCenter: parent.verticalCenter
                            text: "0%"
                            color: Theme.secondaryColor
                            font {
                                pixelSize: Theme.fontSizeLarge
                                family: Theme.fontFamily
                            }
                        }
                    }
                }
            }
        }

        VerticalScrollDecorator {}
    }
}
