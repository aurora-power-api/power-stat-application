/*******************************************************************************
**
** Copyright 2023 Dmitriy Butakov.
**
*******************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    objectName: "cover"

    CoverPlaceholder {
        objectName: "placeholder"
        text: qsTr("Power App")
        icon {
            source: Qt.resolvedUrl("../icons/powerapp.svg")
            sourceSize { width: icon.width; height: icon.height }
        }
        forceFit: true
    }
}

