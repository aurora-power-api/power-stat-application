################################################################################
##
## Copyright 2023 Dmitriy Butakov
##
################################################################################

TARGET = powerapp

QMAKE_CXXFLAGS += -std=c++17

CONFIG += \
    auroraapp \

QT += \
    dbus \
    widgets \

PKGCONFIG += \
    powerstat \

SOURCES += \
    src/appinfo.cpp \
    src/main.cpp \
    src/plotview.cpp \
    src/power.cpp \

HEADERS += \
    src/appinfo.h \
    src/plotview.h \
    src/power.h \

DISTFILES += \
    rpm/powerapp.spec \
    AUTHORS.md \
    README.md \
    LICENSE\

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/powerapp.ts \
    translations/powerapp-ru.ts \
