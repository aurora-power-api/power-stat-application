<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>CoverPage</name>
    <message>
        <location filename="../qml/cover/CoverPage.qml" line="15"/>
        <source>Power App</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerAppMainPage</name>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="18"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="27"/>
        <source>%1m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="32"/>
        <source>%1h %2m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="35"/>
        <source>1 day %2h %3m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="38"/>
        <source>%1 days %2h %3m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="82"/>
        <source>Battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="97"/>
        <source>Last charged to 100%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="117"/>
        <source>Time left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="133"/>
        <source>Battery statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="206"/>
        <source>Disable user statistics collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="207"/>
        <source>Enable user statistics collection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="208"/>
        <source>For better battery estimation, user statistics should be considered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="214"/>
        <source>Application statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="222"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="225"/>
        <source>Battery Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/PowerAppMainPage.qml" line="228"/>
        <source>Applications Activity</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
